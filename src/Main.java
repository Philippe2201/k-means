
import org.jfree.chart.ChartUtilities;
import org.jfree.data.xy.XYSeriesCollection;


/**
 *
 * @author Philippe
 */
public class Main {
    
    // -- variavel para armazenar as dimensoes do conjunto de dados --
    private static final int nDimensoes = 2;
    
    // -- variavel para armazenar a quantidade de exemplos do conjunto de dados --
    private static final int nExemplos = 50;
    
    // -- variavel para armazenar o numero de clusteres diferentes --
    private static final int nClusteres = 3;
    
    public static void main(String[] args) {
        String caminhoIris = "/Volumes/Files HD/Development/USP/K-means/iris.csv";
        String caminhoPhil = "/Volumes/Files HD/Development/USP/K-means/ConjuntoDadosT1.csv";
        String caminhoFe = "C:\\Users\\Felipe\\Documents\\NetBeansProjects\\k-means\\ConjuntoDadosT1.csv";
        readInputFile reader = new readInputFile(caminhoPhil, nExemplos, nDimensoes, nClusteres);
        
        double[][] dados = reader.read();
        //int[][] targets = reader.getTarget();
        
        KMeans kmeans = new KMeans();

        double[][] centroides;
        int[] alocacao;
        int[] novaAlocacao;
        int indiceIteracao;
        
        for(int i=0; i<3; i++){
            indiceIteracao = 0;
            alocacao = new int[dados.length];
            novaAlocacao = new int[dados.length];
            centroides = kmeans.inicializaCentroides(nClusteres, nDimensoes, dados);
            do{
                alocacao = kmeans.alocarObjetos(dados, centroides);

                // para gerar o grafico
                Graficos graf = new Graficos(nClusteres);
                XYSeriesCollection dataset = graf.CreateXYDataset(dados, alocacao, centroides);
                graf.showGraphic(dataset, indiceIteracao, i+1);
                indiceIteracao++;

                centroides = kmeans.atualizarPrototipos(centroides, alocacao, dados);
                novaAlocacao = kmeans.alocarObjetos(dados, centroides);
            }while(kmeans.houveMudancas(novaAlocacao, alocacao));
            System.out.println("Erro execucao " + (i+1) + ": " + kmeans.calculaErroQuadraticoMedio(dados, centroides, alocacao));
            //confereRespostas(targets, alocacao);
        }
        
        //imprimeCentroides(centroides);
        //imprimeAlocacao(novaAlocacao);
    }
    
    static void imprimeCentroides(double[][] centroides){
        System.out.println("------CENTROIDES------");
        for(int i=0; i<centroides.length; i++){
            for(int j=0; j<centroides[i].length; j++){
                System.out.print(centroides[i][j] + ", ");
            }
            System.out.println();
        }
    }
    
    static void imprimeAlocacao(int[] alocacao){
        System.out.println("-------ALOCACAO-------");
        for(int i=0; i<alocacao.length; i++){
            System.out.println(alocacao[i]);
        }
    }
    
    static void confereRespostas(int[][] targets, int[] alocacao){
        int nAcertos = 0;
        for(int i=0; i<alocacao.length; i++){
            int valor = 0;
            for(int j=0; j<targets[i].length; j++){
                if(targets[i][j] == 1){
                    valor = j;
                }
            }
            if(valor == alocacao[i]){
                nAcertos++;
            }            
        }
        double acertosP = nAcertos;
        double exemplosP = nExemplos;
        double porcentagem = (acertosP/exemplosP)*100;
        System.out.println("Numero de acertos: " + nAcertos);
        System.out.println("Porcentagem de acertos: " + porcentagem);
    }
    
    
    
}
