
import java.util.Random;

/**
 *
 * @author Philippe
 */
public class KMeans {
    
    double[][] inicializaCentroides(int nClusteres, int nDimensoes, double[][] dados){
        double[][] centroides;
        centroides = new double[nClusteres][nDimensoes];
        Random random = new Random();
        
        // -- inicializa cada uma das posicoes do array com um numero aleatorio entre 0 e 1 --
        System.out.println("CENTRÓIDES INICIAIS");
        for(int i=0; i<centroides.length; i++){
            int posicao = random.nextInt(dados.length);
            System.out.print("Cluster " + (i+1) + ": ");
            for(int j=0; j<centroides[i].length; j++){
                centroides[i][j] = dados[posicao][j];
                System.out.print(centroides[i][j] + " ");
            }
            System.out.println();
        }
        
        return centroides;
    }
    
    
    /*double[][] inicializaCentroides(int nClusteres, int nDimensoes, double[][] dados){
        double[][] centroides = new double[nClusteres][];
        double[] um = {5.1,3.5,1.4,0.2};
        double[] dois = {7.0,3.2,4.7,1.4};
        double[] tres = {6.3,3.3,6.0,2.5};
        centroides[0] = um;
        centroides[1] = dois;
        centroides[2] = tres;
        return centroides;
    }*/
    
    int[] alocarObjetos(double[][] dados, double[][] centroides){
        // -- vetor de alocacao dos vetores --
        int[] alocacao = new int[dados.length];
        
        // -- para cada um dos vetores de entrada --
        for(int i=0; i<dados.length; i++){
            
            double distanciaMinima = Double.MAX_VALUE;
            // -- para cada um dos centroides encontra a distancia minima --
            for(int c=0; c<centroides.length; c++){
                // -- calcula a distancia do centroide para o vetor atual --
                double distanciaAtual = calcularDistancia(centroides[c], dados[i]);
                // -- se a distancia for menor --
                if(distanciaAtual < distanciaMinima){
                    // -- atualiza o valor da distancia minima --
                    distanciaMinima = distanciaAtual;
                    // -- seta o vetor i como sendo percencente ao cluster c --
                    alocacao[i] = c;
                }
            }
        }
        return alocacao;
    }
    
    double[][] atualizarPrototipos(double[][] centroides, int[] alocacao, double[][] dados){
        double[][] novosCentroides = new double[centroides.length][centroides[0].length];
        // -- para cada um dos centroides --
        for(int indiceCentroide=0; indiceCentroide<centroides.length; indiceCentroide++){
            // -- array para armazenar o novo centroide --
            double[] novoCentroide = new double[centroides[0].length];
            // -- variavel para armazenar a quantidade de vetores pertencentes ao centroide atual --
            int quantidade = 0;
            for(int k=0; k<alocacao.length; k++){
                // -- se o vetor foi alocado ao centroide atual --
                if(verificaSePertence(alocacao[k], indiceCentroide)){
                    // -- incrementa a quantidade de vetores --
                    quantidade++;
                    // -- soma o novoCentroide com os valores do vetor atual --
                    somaCentroide(novoCentroide, dados[k]);
                }
            }
            // -- atualiza o centroide --
            novosCentroides[indiceCentroide] = calculaMediaCentroide(novoCentroide, quantidade);
        }
        return novosCentroides;
    }
    
    boolean verificaSePertence(int alocacao, int indiceCentroide){
        if(alocacao == indiceCentroide){
            return true;
        }
        return false;
    }
    
    double[] somaCentroide(double novoCentroide[], double vetor[]){
        for(int i=0; i<novoCentroide.length; i++){
            novoCentroide[i] += vetor[i];
        }
        return novoCentroide;
    }
    
    // -- metodo para calcular a distancia entre dois vetores --
    double calcularDistancia(double[] x, double[] y){
        double soma = 0;
        for(int i=0; i<x.length; i++){
            soma += Math.pow((x[i]-y[i]), 2);
        }
        return Math.sqrt(soma);
    }
    
    double[] calculaMediaCentroide(double[] novoCentroide, int quantidade){
        for(int i=0; i<novoCentroide.length; i++){
            novoCentroide[i] = novoCentroide[i]/quantidade;
        }
        return novoCentroide;
    }
    
    // -- verifica se houve mudancas na alocacao dos vetores --
    boolean houveMudancas(int[] alocacaoAntiga, int[] alocacaoAtual){
        for(int i=0; i<alocacaoAntiga.length; i++){
            if(alocacaoAntiga[i] != alocacaoAtual[i]){
                return true;
            }
        }
        return false;
    }
    
    double calculaErroQuadraticoMedio(double[][] dados, double[][] centroides, int[] alocacao){
        // -- variavel para armazenar o erro quadratico medio --
        double erro = 0;
        
        // -- para cada um dos centroides --
        for(int indiceCentroide=0; indiceCentroide<centroides.length; indiceCentroide++){
            // -- varre o vetor de alocacoes verificando quais dados foram alocados para o centroide atual --
            for(int k=0; k<alocacao.length; k++){
                // -- se o vetor foi alocado ao centroide atual --
                if(verificaSePertence(alocacao[k], indiceCentroide)){
                    // -- calcula a ditancia do vetor para o centroide --
                    erro += Math.pow(calcularDistancia(dados[k], centroides[indiceCentroide]), 2);
                }
            }
            
        }
        
        return erro;
    }
    
}
