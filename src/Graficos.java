
import java.awt.Color;
import java.awt.Shape;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.util.ShapeUtilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Felipe
 */
public class Graficos {
    
    private int nclusters;
    
    public Graficos(int nClusters){
        this.nclusters = nClusters;
    }
    
    public XYSeriesCollection CreateXYDataset(double[][] dados, int[] labels, double[][] centroides){
        
        XYSeries[] series = new XYSeries[nclusters+1];
        
        // inicializa os series, que seria os dados dividos entre os clusters
        for(int k = 0; k < nclusters; k++){
            series[k] = new XYSeries("Cluster "+(k+1));
        }
        // adiciona os dados nas series XYSeries
        for(int i = 0; i<labels.length; i++){
            series[labels[i]].add(dados[i][0], dados[i][1]);
        }
        
        // series para os centroides
        series[nclusters] = new XYSeries("Centroides");
        for(int i=0; i<centroides.length;i++){
             series[nclusters].add(centroides[i][0], centroides[i][1]);
        }
        
        // cria colecao de dados
        XYSeriesCollection dadosGrafico = new XYSeriesCollection();
        // adicionas os series nessa colecao de dados
        for(int k=0; k<series.length; k++){
            dadosGrafico.addSeries(series[k]);
        }
              
        return dadosGrafico;
        
    }
    
    public void showGraphic(XYSeriesCollection dados, int iteracao, int execucao){
        
        JFreeChart chart = createChart(dados);
        TextTitle title = new TextTitle("Execução " + execucao + " | Iteração " + iteracao);
        chart.addSubtitle(title);
        chart.setBackgroundPaint(Color.decode("#FAFAD2"));
        Plot plot = chart.getPlot();
        plot.setBackgroundPaint(Color.white);
        
        ChartFrame chartPanel = new ChartFrame("Grafico",chart);
        //ChartPanel chartPanel = new ChartPanel(chart);
        //chartPanel.setVerticalAxisTrace(true);
        //chartPanel.setHorizontalAxisTrace(true);
        // popup menu conflicts with axis trace
        //chartPanel.setPopupMenu(null);
       
        //chartPanel.setDomainZoomable(true);
        //chartPanel.setRangeZoomable(true);
        
        //chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        //setContentPane(chartPanel);
        chartPanel.setResizable(true);
        chartPanel.pack();
        //chartPanel.setVisible(true);
        try {
            ChartUtilities.saveChartAsPNG(new File("3/" + execucao + "-" + iteracao + ".png"), chart, 1000, 500);
        } catch (IOException ex) {
            Logger.getLogger(Graficos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private JFreeChart createChart(XYSeriesCollection dados){
        
        JFreeChart chart =  ChartFactory.createScatterPlot("Distribuição dos Clusters", "Dim 1", "Dim 2", dados,PlotOrientation.VERTICAL, true, false, false);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setNoDataMessage("NO DATA");
        plot.setDomainZeroBaselineVisible(true);
        plot.setRangeZeroBaselineVisible(true);
        
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        
        // -- para os centroides --
        renderer.setSeriesOutlinePaint(nclusters, Color.GREEN);
        Shape meuShape = ShapeUtilities.createDiagonalCross(7, 0.5f);
        renderer.setSeriesShape(nclusters, meuShape);

        // -- preenchimento dos pontos --
        renderer.setSeriesPaint(0, Color.yellow);
        
        renderer.setBaseFillPaint(Color.yellow);

        renderer.setUseOutlinePaint(true);
        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setTickMarkInsideLength(2.0f);
        domainAxis.setTickMarkOutsideLength(0.0f);
       
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickMarkInsideLength(2.0f);
        rangeAxis.setTickMarkOutsideLength(0.0f);
        
        return chart;
    }
}
